# Ruffus: a lightweight Python library for computational pipelines

# Краткое описание
Ruffus — это библиотека Computation Pipeline для Python. Он с открытым исходным кодом, мощный и удобный для пользователя, и широко используется в науке и биоинформатике.

#### Литература:
1. Leo Goodstadt (2010) : Ruffus: a lightweight Python library for computational pipelines. Bioinformatics 26(21): 2778-2779

2. Altschul SF, et al. Basic local alignment search tool, J. Mol. Biol., 1990, vol. 215 (pg. 403-410)

3. Cock PJ, et al. Biopython: freely available Python tools for computational molecular biology and bioinformatics, Bioinformatics, 2009, vol. 25 (pg. 1422-1423)

3. Hoon S, et al. Biopipe: a flexible framework for protocol-based bioinformatics analysis, Genome Res., 2003, vol. 13 (pg. 1904-1915)

4. Oinn T, et al. Taverna: a tool for the composition and enactment of bioinformatics workflows, Bioinformatics, 2004, vol. 20 (pg. 3045-3054)

5. Shah SP, et al. Pegasys: software for executing and integrating analyses of biological sequences, BMC Bioinformatics, 2004, vol. 5 pg. 40 